import React, { useState } from 'react'
import {dataShoe} from './data_shoe'

export default function HookShoe() {
    const [shoeArr, setShoeArr] = useState(dataShoe);
    console.log(setShoeArr);
    let renderShoeList = () => {
        return shoeArr.map((item) => {
            return (
            
                <div className="col-3 p-4">
                  <div className="card ">
                    <img src={item.image} className="card-img-top" alt="..." />
                    <div className="card-body">
                      <h5 className="card-title">{item.name}</h5>
                      <p className="card-text">{item.price}</p>
                      <a
                        onClick={() => {
                          handleOnclick(item.id);
                        }}
                        href="#"
                        className="btn btn-primary"
                      >
                        Add to card
                      </a>
                    </div>
                  </div>
                </div>
              );
        })
    }
    let handleOnclick = (id) =>{
        console.log(id)
        let cart = [];
        console.log(cart);
        let index = cart.findIndex((item) => {
            return item.id == id.id;
        })
        console.log(index);
        if(index == -1){
            let newShoe = [...id];
            console.log(newShoe);
            cart.push(newShoe);
        }

        console.log(cart);
        
    }
    let renderTbody = () => {
        return shoeArr.map((item) => {
          return (
            <tr>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>
                <button className="btn btn-danger">-</button>
                <strong className="mx-3">{item.soLuong}</strong>
                <button className="btn btn-success">+</button>
              </td>
              <td>{item.price * item.soLuong}</td>
              <td>
                <img src={item.image} style={{ width: 50 }} alt="" />
              </td>
            </tr>
          );
        });
      };
  return (
    
    <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Img</th>
          </thead>
          <tbody>{renderTbody()}</tbody>
        </table> 
      <div className="row">
      {renderShoeList()}
      </div>
    </div>
  )
}
